app.controller("pembelianCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
     var tableStateRef;
     $scope.formtittle = "";
     $scope.displayed = [];
     $scope.form = {};
     $scope.is_edit = false;
     $scope.is_view = false;
     $scope.is_create = false;
     $scope.loading = false;
    /**
     * End inialisasi
     */
     $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("apppembelian/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
                );
        });
        $scope.isLoading = false;
    };
    $scope.getDetail = function (id) {
        Data.get("apppembelian/view?t_pembelian_id=" + id).then(function (response) {
            $scope.listDetail = response.data;
        });
    };
    $scope.listDetail = [{
            no: ""
        }];
    $scope.addDetail = function (newdet = {no:''}) {
        var val = $scope.listDetail.length;
        var newDet = newdet;
        console.log(newDet);
        $scope.listDetail.push(newDet);
    };
    $scope.removeDetail = function (paramindex) {
        var r = confirm('Apakah Anda ingin menghapus item ini?');
            if (r) {
                $scope.listDetail.splice(paramindex, 1);
            }
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tanggal = new Date();
        $scope.listDetail = [];
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.tanggal = new Date (form.tanggal);
        $scope.getDetail(form.id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.tanggal = new Date (form.tanggal);
        $scope.getDetail(form.id);
    };
    $scope.save = function (form) {
        $scope.loading = true;
        var form = {
            data: form,
            detail: $scope.listDetail
        }
        Data.post("apppembelian/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post("apppembelian/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post("apppembelian/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    Data.get("appsuplier/index").then(function(data) {
        $scope.listSuplier = data.data.list;
    });
    Data.get("appbarang/index").then(function(data) {
        $scope.listBarang = data.data.list;
        // console.log(data.data.list );
    });
    $scope.hitungTotal = function(){
        $scope.form.total = 0;
        angular.forEach($scope.listDetail, function(value, key) {
            value.sub_total = value.jumlah*value.harga;
            $scope.form.total +=  value.sub_total;
        });
    };
    $scope.addBrg = function() {
        var form = "";
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_pembelian/modalbrg.html",
            controller: "addBrgCtrl",
            size: "lg",
            backdrop: "static",
            resolve: {
                form: function() {
                    return form;
                }
            }
        });
        modalInstance.result.then(function(result) {
            // console.log(result)
        if (result == undefined) {
            console.log("kosong")
            } else {
                $scope.addDetail({no: '', id: result.id, nama: result.nama, satuan: result.satuan})
            }

      }, function() {});
    };

});
app.controller("addBrgCtrl", function($state, $scope, Data, $uibModalInstance, form, $rootScope) {
    $scope.form = {};
    Data.get("appsuplier/index").then(function(data) {
        $scope.listSuplier = data.data.list;
    });
    Data.get("appsatuan/index").then(function(data) {
        $scope.listSatuan = data.data.list;
    });
    Data.get("appkategori/index").then(function(data) {
        $scope.listKategori = data.data.list;
    });
    $scope.saveBrg = function(form) {
        var data = form;
        var newDet = {
            val: data.nama
        };
        // console.log(data);
        var url = form.id > 0 ? "/update" : "/save";
        Data.post("appbarang" + url, data).then(function(result) {
         if (result.status_code == 200) {
            Data.get("apppembelian/getProduk", newDet).then(function(response) {
                // console.log(response.data.data[0])
                $uibModalInstance.close(response.data.data[0]);   });
                swal("Berhasil", "Data Berhasil Disimpan", "success");
            } else {
                toaster.pop("error", "Terjadi Kesalahan", result.erros);
            }
        });
    };
    $scope.close = function() {
        $uibModalInstance.dismiss("cancel");
    };
});
