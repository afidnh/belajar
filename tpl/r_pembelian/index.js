app.controller("RPembelianCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    $scope.pivot = function (form) {
        moment(form.tanggal).format('YYYY-MM-DD');
        $scope.is_laporan = true;

        Data.get("rpembelian/laporan", form).then(function (response) {
          console.log('aasd', response);
          $scope.colspan        = response.data.panjangTanggal;
          $scope.listTanggal    = response.data.tanggal;
          $scope.data           = response.data.list;
          $scope.totalPerbarang = response.data.totalPerbarang;
          $scope.is_Excel       = true;
          console.log(response.data);
          console.log($scope.listTanggal);
        });
    };

    $scope.clear = function () {
        $scope.form.tanggal = '';
    };

    $scope.dataExcel = function dataExcel() {
        $scope.is_Excel = false;
    };

    $scope.export = function (jenisReport) {
        console.log(jenisReport);

        var blob = new Blob([document.getElementById('print-area' + jenisReport).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Laporan-Personal-Commission.xls");
    }


});
