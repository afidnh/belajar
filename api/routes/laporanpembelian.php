<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/laporanpembelian/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("t_pembelian_det.id,
                t_pembelian_det.t_pembelian_id,
                t_pembelian_det.m_barang_id,
                t_pembelian_det.jumlah,
                t_pembelian_det.harga,
                t_pembelian_det.sub_total,
                t_pembelian_det.is_deleted,
                t_pembelian.id,
                t_pembelian.tanggal,
                t_pembelian.invoice,
                t_pembelian.m_suplier_id,
                t_pembelian.total,
                t_pembelian.is_deleted,
                m_barang.nama as barang,
                m_suplier.nama as suplier")
        ->from("t_pembelian_det")
        ->join("left join", "t_pembelian", "t_pembelian_det.t_pembelian_id = t_pembelian.id")
         ->join("left join", "m_barang", "t_pembelian_det.m_barang_id = m_barang.id")
         ->join("left join", "m_suplier", "t_pembelian.m_suplier_id = m_suplier.id")
        ->where("t_pembelian_det.is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }else if (isset($params["suplier"]) && !empty($params["suplier"])) {
                $db->where("m_suplier.nama", "LIKE", $params["suplier"]);
            }
    $models = $db->findAll();
    foreach ($models as $val) {
            $data['totalPembelian'] += $val->sub_total;
        }
    return successResponse($response,[ 'list' => $models, 'totalPembelian' => $data['totalPembelian'],]);
});

$app->get("/laporanpembelian/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("t_pembelian_det.id,
                t_pembelian_det.t_pembelian_id,
                t_pembelian_det.m_barang_id,
                t_pembelian_det.jumlah,
                t_pembelian_det.harga,
                t_pembelian_det.sub_total,
                t_pembelian_det.is_deleted,
                t_pembelian.id,
                t_pembelian.tanggal,
                t_pembelian.invoice,
                t_pembelian.m_suplier_id,
                t_pembelian.total,
                t_pembelian.is_deleted,
                m_barang.nama as barang,
                m_suplier.nama as suplier")
        ->from("t_pembelian_det")
        ->join("left join", "t_pembelian", "t_pembelian_det.t_pembelian_id = t_pembelian.id")
         ->join("left join", "m_barang", "t_pembelian_det.m_barang_id = m_barang.id")
         ->join("left join", "m_suplier", "t_pembelian.m_suplier_id = m_suplier.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_barang.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("t_pembelian_det.is_deleted", "=", $val);
            } else if ($key == "suplier") {
                $db->where("m_suplier.nama", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
     $data['totalPembelian'] =0;
   foreach ($models as $val) {
            $data['totalPembelian'] += $val->sub_total;
        }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem, 'totalPembelian' => $data,]);
});