<?php
/**
 * Ambil semua
 */
$app->get("/rpembelian/laporan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    date_default_timezone_set("Asia/Jakarta");

    $db->select("
      t_pembelian.tanggal,
      Sum(t_pembelian_det.jumlah) AS total,
      m_barang.nama as barang,
      m_barang.id
    ")
    ->from("t_pembelian_det")
    ->join("left join", "t_pembelian", "t_pembelian_det.t_pembelian_id = t_pembelian.id")
    ->join("left join", "m_barang", "t_pembelian_det.m_barang_id = m_barang.id");

    if (isset($params["tanggal"]) && $params["tanggal"] != null) {
      $bulan = date("m", strtotime($params['tanggal']));
      $tahun = date("Y", strtotime($params['tanggal']));
      $db->where("MONTH(t_pembelian.tanggal)", "=", $bulan);
      $db->andWhere("YEAR(t_pembelian.tanggal)", "=", $tahun);
      $db->groupBy("m_barang.id, t_pembelian.tanggal");
    }

    $models = $db->findAll();

    // Mengelompokkan tanggal pembelian per produk
    $listpembelian = $totalPerbarang = [];
    foreach ($models as $key => $value) {
      $listpembelian[$value->id]['id']            = $value->id;
      $listpembelian[$value->id]['nama']          = $value->barang;
      $listpembelian[$value->id]['tanggal'][$value->tanggal] = $value->total;
      @$totalPerbarang[$value->id]                += $value->total;
    }
    // Mengelompokkan tanggal pembelian per produk - END

    // Buat array list tanggal
    $hari=cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
    $listTanggal = [];
    for($i = 1; $i <=  $hari; $i++){
       $tanggal = $tahun . "-" .  $bulan . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
       $listTanggal[] = $tanggal;
    }
    // Buat array list tanggal - END

    // Inisiasi pembelian per tanggal
    foreach ($listpembelian as $key => $value) {
      foreach ($listTanggal as $value2) {
        if( !isset($listpembelian[$key]['tanggal'][$value2]) ){
          $listpembelian[$key]['tanggal'][$value2] = 0;
        }
      }
      // Mengurutkan value tanggal
      ksort($listpembelian[$key]['tanggal']);
    }

    // Inisiasi pembelian per tanggal

    return successResponse($response, [
      "list"            => $listpembelian,
      "totalPerbarang"  => $totalPerbarang,
      "tanggal"         => $listTanggal,
      "panjangTanggal"  => sizeof($listTanggal)
    ]);
});

