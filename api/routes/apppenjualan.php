<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "tanggal" => "required",
        "invoice" => "required",
        
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil detail
 */
$app->get("/apppenjualan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_penjualan_det.id,
                t_penjualan_det.t_penjualan_id,
                t_penjualan_det.m_barang_id,
                t_penjualan_det.jumlah,
                t_penjualan_det.harga,
                t_penjualan_det.sub_total,
                t_penjualan_det.is_deleted,
                t_penjualan.id,
                t_penjualan.tanggal,
                t_penjualan.invoice,
                t_penjualan.m_suplier_id,
                t_penjualan.total,
                t_penjualan.is_deleted,
                m_barang.nama as barang,
                m_suplier.nama as suplier")
        ->from("t_penjualan_det")
        ->join("left join", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
         ->join("left join", "m_barang", "t_penjualan_det.m_barang_id = m_barang.id")
         ->join("left join", "m_suplier", "t_penjualan.m_suplier_id = m_suplier.id")
        ->where("t_penjualan_id", "=", $params["t_penjualan_id"]);
    $models = $db->findAll();
     foreach ($models as $key => $value) {
        $models[$key]->m_barang_id = [
            'id' => $value->m_barang_id,
            'nama' => $value->barang,

        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua m tindak kekerasan
 */
$app->get("/apppenjualan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_penjualan.id,
                t_penjualan.tanggal,
                t_penjualan.invoice,
                t_penjualan.m_suplier_id,
                t_penjualan.total,
                t_penjualan.is_deleted,
                m_suplier.nama as suplier")
        ->from("t_penjualan")
         ->join("inner join", "m_suplier", "t_penjualan.m_suplier_id = m_suplier.id")
        ->where("t_penjualan.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();
     foreach ($models as $key => $val) {
        $val->m_suplier_id = (string) $val->m_suplier_id;
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m tindak kekerasan
 */
$app->post("/apppenjualan/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            if (isset($data["data"]["id"])) {
                $model = $db->update("t_penjualan", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_penjualan_det", ["t_penjualan_id" => $data["data"]["id"]]);
                
            } else {
                $model = $db->insert("t_penjualan", $data["data"]);
            }
            /**
             * Simpan detail
             */ 
            // print_r($data["detail"]);
            //     die;

            if (isset($data["detail"]) && !empty($data["detail"])) {
                foreach ($data["detail"] as $key => $val) {
                    $detail["id"] = isset($val["id"]) ? $val["id"] : '';
                    $detail["m_barang_id"] = isset($val["m_barang_id"]['id']) ? $val["m_barang_id"]['id'] : '';
                    $detail["jumlah"] = isset($val["jumlah"]) ? $val["jumlah"] : '';
                    $detail["harga"] = isset($val["harga"]) ? $val["harga"] : '';
                    $detail["sub_total"] = isset($val["sub_total"]) ? $val["sub_total"] : '';
                    $detail["t_penjualan_id"] = $model->id;
                    $db->insert("t_penjualan_det", $detail);
                    $db->run("update m_barang set stock = stock - " .$detail["jumlah"]. " where id = ".$detail["m_barang_id"]);
                }
            //     print_r($val);
            // die;
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m tindak kekerasan
 */
$app->post("/apppenjualan/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("t_penjualan", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
