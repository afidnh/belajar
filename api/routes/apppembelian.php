<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */

function validasi($data, $custom = array())
{
    $validasi = array(
        "tanggal" => "required",
        "invoice" => "required",
        
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil detail
 */
$app->get("/apppembelian/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_pembelian_det.id,
        t_pembelian_det.t_pembelian_id,
        t_pembelian_det.m_barang_id,
        t_pembelian_det.jumlah,
        t_pembelian_det.harga,
        t_pembelian_det.sub_total,
        t_pembelian_det.is_deleted,
        t_pembelian.id,
        t_pembelian.tanggal,
        t_pembelian.invoice,
        t_pembelian.m_suplier_id,
        t_pembelian.total,
        t_pembelian.is_deleted,
        m_suplier.nama as suplier,
        m_barang.nama as barang")
        ->from("t_pembelian_det")
        ->join("left join", "t_pembelian", "t_pembelian_det.t_pembelian_id = t_pembelian.id")
         ->join("left join", "m_barang", "t_pembelian_det.m_barang_id = m_barang.id")
         ->join("left join", "m_suplier", "t_pembelian.m_suplier_id = m_suplier.id")
        ->where("t_pembelian_det.t_pembelian_id", "=", $params["t_pembelian_id"]);
    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $models[$key]->m_barang_id = [
            'id' => $value->m_barang_id,
            'nama' => $value->barang,

        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua data
 */
$app->get("/apppembelian/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_pembelian.id,
t_pembelian.tanggal,
t_pembelian.invoice,
t_pembelian.m_suplier_id,
t_pembelian.total,
t_pembelian.is_deleted,
m_suplier.nama as suplier")
        ->from("t_pembelian")
         ->join("inner join", "m_suplier", "t_pembelian.m_suplier_id = m_suplier.id")
        ->where("t_pembelian.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();
     foreach ($models as $key => $val) {
        $val->m_suplier_id = (string) $val->m_suplier_id;
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m tindak kekerasan
 */
$app->post("/apppembelian/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            if (isset($data["data"]["id"])) {
                $model = $db->update("t_pembelian", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_pembelian_det", ["t_pembelian_id" => $data["data"]["id"]]);
                // print_r($data);
                // die;
            } else {
                $model = $db->insert("t_pembelian", $data["data"]);
            }
            /**
             * Simpan detail
             */ 

            if (isset($data["detail"]) && !empty($data["detail"])) {
                foreach ($data["detail"] as $key => $val) {
                    $detail["id"] = isset($val["id"]) ? $val["id"] : '';
                    $detail["m_barang_id"] = isset($val["m_barang_id"]['id']) ? $val["m_barang_id"]['id'] : '';
                    $detail["jumlah"] = isset($val["jumlah"]) ? $val["jumlah"] : '';
                    $detail["harga"] = isset($val["harga"]) ? $val["harga"] : '';
                    $detail["sub_total"] = isset($val["sub_total"]) ? $val["sub_total"] : '';
                    $detail["t_pembelian_id"] = $model->id;
                    $db->insert("t_pembelian_det", $detail);
                    $db->run("update m_barang set stock = stock + " .$detail["jumlah"]. " where id = ".$detail["m_barang_id"]);
                }
            //     print_r($val);
            // die;
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m tindak kekerasan
 */
$app->post("/apppembelian/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("t_pembelian", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/** Pencarian produk */
$app->get('/apppembelian/getProduk', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $models = $db->select("m_barang.*, m_kategori.nama as kategori, m_suplier.nama as suplier, m_satuan.nama as satuan")
        ->from("m_barang")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id = m_kategori.id")
        ->join("left join", "m_suplier", "m_barang.m_suplier_id = m_suplier.id")
        ->join("left join", "m_satuan", "m_barang.m_satuan_id = m_satuan.id")
        ->where("m_barang.is_deleted", "=", 0)
        ->where("m_barang.nama", "=", $params["val"])
        ->findAll();

    return successResponse($response, ['data' => $models]);
});