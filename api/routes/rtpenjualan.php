<?php
/**
 * Ambil semua
 */
$app->get("/rtpenjualan/laporan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    date_default_timezone_set("Asia/Jakarta");

    $db->select("
      DATE_FORMAT(t_penjualan.tanggal,'%m-%Y') AS bulan,
      Sum(t_penjualan_det.sub_total) AS total,
      m_barang.nama as barang,
      m_barang.id
    ")
    ->from("t_penjualan_det")
    ->join("left join", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
    ->join("left join", "m_barang", "t_penjualan_det.m_barang_id = m_barang.id");

    if (isset($params["tanggal"]) && $params["tanggal"] != null) {
      $tahun = date("Y", strtotime($params['tanggal']));
      $db->where("YEAR(t_penjualan.tanggal)", "=", $tahun);
      $db->groupBy("m_barang.id, MONTH(t_penjualan.tanggal)");
    }

    $models = $db->findAll();

    // Mengelompokkan tanggal pembelian per produk
    $listpembelian = $totalPerbarang = [];
    foreach ($models as $key => $value) {
      $listpembelian[$value->id]['id']            = $value->id;
      $listpembelian[$value->id]['nama']          = $value->barang;
      $listpembelian[$value->id]['bulan'][$value->bulan] = $value->total;
      @$totalPerbarang[$value->id]                += $value->total;
      @$totalPerbulan[$value->bulan] += $value->total;
    }
    // Mengelompokkan tanggal pembelian per produk - END

    // Buat array list tanggal
    // $listBulan = array('01-'.$tahun,'02-'.$tahun,'03-'.$tahun,'04-'.$tahun,'05-'.$tahun,'06-'.$tahun,'07-'.$tahun,'08-'.$tahun,'09-'.$tahun,'10-'.$tahun,'11-'.$tahun,'12-'.$tahun);
    for($m=1; $m<=12; $m++){
       $bulan =DateTime::createFromFormat('m-Y',$m.'-'.$tahun)->format('m-Y');
       $listBulan[] = $bulan;
    }
    // Buat array list tanggal - END

    // Inisiasi pembelian per tanggal
    foreach ($listpembelian as $key => $value) {
      foreach ($listBulan as $value2) {
        if( !isset($listpembelian[$key]['bulan'][$value2]) ){
          $listpembelian[$key]['bulan'][$value2] = 0;
        }
      }
      // Mengurutkan value tanggal
      ksort($listpembelian[$key]['bulan']);
    }

    // Inisiasi pembelian per bulan
    foreach ($totalPerbulan as $key => $value) {
      foreach ($listBulan as $value2) {
        if( !isset($totalPerbulan[$value2]) ){
          $totalPerbulan[$value2] = 0;
        }
      }
      // Mengurutkan value tanggal
      ksort($totalPerbulan);
    }

    return successResponse($response, [
      "list"            => $listpembelian,
      "totalPerbulan"   => $totalPerbulan,
      "totalPerbarang"  => $totalPerbarang,
      "bulan"         => $listBulan,
      "panjangBulan"  => sizeof($listBulan)
    ]);
});

