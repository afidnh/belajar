<?php
/**
 * Ambil semua
 */
$app->get("/laporanpenjualan/laporan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    date_default_timezone_set("Asia/Jakarta");

    $db->select("t_penjualan.tanggal,
                Sum(t_penjualan_det.jumlah) AS total,
                m_barang.nama as barang,
                m_barang.id")
        ->from("t_penjualan_det")
        ->join("left join", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id = m_barang.id");
if (isset($params["tanggal"]) && $params["tanggal"] != null) {
        $bulan = date("m", strtotime($params['tanggal']));
        $tahun = date("Y", strtotime($params['tanggal']));
        $db->where("MONTH(t_penjualan.tanggal)", "=", $bulan);
        $db->andWhere("YEAR(t_penjualan.tanggal)", "=", $tahun);
        $db->groupBy("m_barang.nama, t_penjualan.tanggal");


    }

    $models = $db->findAll();
    $hari=cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);


    foreach ($models as $key => $value) {
        $dates= array();
         for($i = 1; $i <=  $hari; $i++)
            {
                $tanggal = $tahun . "-" .  $bulan . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
                if ($tanggal == $value->tanggal) {
                     $dates[] =   [

                                     $tanggal => $value->total,
                                 ];
                } else{
                    $dates[] = [

                                     $tanggal => 0,
                                 ];
                }

            }


        $models[$key]->tanggal =$dates;

    }
for($i = 1; $i <=  $hari; $i++)
            {
                $tannggal[] = [ 'tanggal' => $tahun . "-" .  $bulan . "-" . str_pad($i, 2, '0', STR_PAD_LEFT)];
            }

    return successResponse($response, ["list" => $models, "tanggal" => $tannggal]);
});

$app->get("/laporanpenjualan/laporanFix", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    date_default_timezone_set("Asia/Jakarta");

    $db->select("
      t_penjualan.tanggal,
      Sum(t_penjualan_det.jumlah) AS total,
      m_barang.nama as barang,
      m_barang.id
    ")
    ->from("t_penjualan_det")
    ->join("left join", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
    ->join("left join", "m_barang", "t_penjualan_det.m_barang_id = m_barang.id");

    if (isset($params["tanggal"]) && $params["tanggal"] != null) {
      $bulan = date("m", strtotime($params['tanggal']));
      $tahun = date("Y", strtotime($params['tanggal']));
      $db->where("MONTH(t_penjualan.tanggal)", "=", $bulan);
      $db->andWhere("YEAR(t_penjualan.tanggal)", "=", $tahun);
      $db->groupBy("m_barang.id, t_penjualan.tanggal");
    }

    $models = $db->findAll();

    // Mengelompokkan tanggal penjualan per produk
    $listPenjualan = $totalPerbarang = [];
    foreach ($models as $key => $value) {
      $listPenjualan[$value->id]['id']            = $value->id;
      $listPenjualan[$value->id]['nama']          = $value->barang;
      $listPenjualan[$value->id]['tanggal'][$value->tanggal] = $value->total;
      @$totalPerbarang[$value->id]                += $value->total;
    }
    // Mengelompokkan tanggal penjualan per produk - END

    // Buat array list tanggal
    $hari=cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
    $listTanggal = [];
    for($i = 1; $i <=  $hari; $i++){
       $tanggal = $tahun . "-" .  $bulan . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
       $listTanggal[] = $tanggal;
    }
    // Buat array list tanggal - END

    // Inisiasi Penjualan per tanggal
    foreach ($listPenjualan as $key => $value) {
      foreach ($listTanggal as $value2) {
        if( !isset($listPenjualan[$key]['tanggal'][$value2]) ){
          $listPenjualan[$key]['tanggal'][$value2] = 0;
        }
      }
      // Mengurutkan value tanggal
      ksort($listPenjualan[$key]['tanggal']);
    }

    // Inisiasi Penjualan per tanggal

    return successResponse($response, [
      "list"            => $listPenjualan,
      "totalPerbarang"  => $totalPerbarang,
      "tanggal"         => $listTanggal,
      "panjangTanggal"  => sizeof($listTanggal)
    ]);
});
