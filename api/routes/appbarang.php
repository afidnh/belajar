<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama"       => "required",
        "m_satuan_id" => "required",
        "m_kategori_id" => "required",
        "stock"   => "required",
         "m_suplier_id" => "required",
    );
    GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
    // var_dump($data);
}
/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/appbarang/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_barang")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil data user untuk update profil
 */
$app->get("/appbarang/view", function ($request, $response) {
    $db   = $this->db;
    $data = $db->find("select * from m_barang where id = '" . $_SESSION["user"]["id"] . "'");
    return successResponse($response, $data);
});
/**
 * Ambil semua list user
 */
$app->get("/appbarang/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("m_barang.*, m_kategori.nama as kategori, m_suplier.nama as suplier, m_satuan.nama as satuan")
        ->from("m_barang")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id = m_kategori.id")
        ->join("left join", "m_suplier", "m_barang.m_suplier_id = m_suplier.id")
        ->join("left join", "m_satuan", "m_barang.m_satuan_id = m_satuan.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_barang.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_barang.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $key => $val) {
        $val->m_kategori_id = (string) $val->m_kategori_id;
        $val->m_suplier_id = (string) $val->m_suplier_id;
        $val->m_satuan_id = (string) $val->m_satuan_id;
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/appbarang/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_barang", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/appbarang/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
